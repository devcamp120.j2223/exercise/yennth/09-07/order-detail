import { Button, Container, Grid, Typography, Modal, Box, OutlinedInput, MenuItem, Select, Snackbar, Alert } from "@mui/material";
import { useEffect, useState } from "react";

function UpdateModal({ drinkList, openModalEdit, handleCloseEdit, style, getData, setVarRefeshPage, varRefeshPage, rowClicked }) {


    const [openAlert, setOpenAlert] = useState(false)
    const [statusModal, setStatusModal] = useState("error")
    const [noidungAlert, setNoidungAlert] = useState("")

    const [idEdit, setIdEdit] = useState("");
    const [menu, setMenu] = useState('');
    const [pizzaType, setPizzaType] = useState("");
    const [duongKinh, setDuongKinh] = useState("");
    const [suonNuong, setSuonNuong] = useState("");
    const [salad, setSalad] = useState("");
    const [soLuongNuoc, setSoLuongNuoc] = useState("");
    const [thanhTien, setThanhTien] = useState("");
    const [voucher, setVoucher] = useState("");
    const [drink, setDrink] = useState("");
    const [name, setName] = useState("");
    const [email, setEmail] = useState("");
    const [phoneNumber, setPhoneNumber] = useState("");
    const [address, setAddress] = useState("");
    const [message, setMessage] = useState("");
    const [giamGia, setGiamGia] = useState(0);
    const [orderStatus, setOrderStatus] = useState("open");

    useEffect(() => {

        setIdEdit(rowClicked.id)
        setMenu(rowClicked.kichCo)
        setPizzaType(rowClicked.loaiPizza)
        setDuongKinh(rowClicked.duongKinh)
        setSuonNuong(rowClicked.suon)
        setSalad(rowClicked.salad)
        setSoLuongNuoc(rowClicked.soLuongNuoc)
        setThanhTien(rowClicked.thanhTien)
        setVoucher(rowClicked.idVourcher)
        setDrink(rowClicked.idLoaiNuocUong)
        setName(rowClicked.hoTen)
        setEmail(rowClicked.email)
        setPhoneNumber(rowClicked.soDienThoai)
        setAddress(rowClicked.diaChi)
        setMessage(rowClicked.loiNhan)
        setGiamGia(rowClicked.giamGia)
    }, [rowClicked])

    const handleChangeStatus = (event) => {
        setOrderStatus(event.target.value);
    };
    const handleCloseAlert = () => {
        setOpenAlert(false)
    }
    const onBtnUpdateClick = () => {
  
        var vDataForm = {
            trangThai: orderStatus
        }

        const body = {
            method: 'PUT',
            body: JSON.stringify(vDataForm),
            headers: {
                'Content-type': 'application/json; charset=UTF-8',
            },
        }

        getData('http://42.115.221.44:8080/devcamp-pizza365/orders/' + idEdit, body)
            .then((data) => {

                setOpenAlert(true);
                setNoidungAlert("Update trạng thái đơn hàng thành công!")
                setStatusModal("success")
                setVarRefeshPage(varRefeshPage + 1)
                handleCloseEdit()
            })
            .catch((error) => {

                setOpenAlert(true);
                setNoidungAlert("Update trạng thái đơn hàng thất bại!")
                setStatusModal("error")
                handleCloseEdit()
            })

    }
    return (
        <Container>
            <Modal
                open={openModalEdit}
                onClose={handleCloseEdit}
                aria-labelledby="modal-modal-title"
                aria-describedby="modal-modal-description"
            >
                <Box sx={{ ...style, width: 800 }}>
                    <Typography variant="h5" component="h2" mb={2} textAlign="center">
                        Cập nhật trạng thái đơn hàng
                    </Typography>
                    <Grid container >
                        <Grid item xs={6} md={6} sm={6} lg={6} p={1}>
                            <Grid container spacing={1}>
                                <Grid item xs={4} md={4} sm={4} lg={4} marginY={"auto"}>
                                    <Typography variant="h6" component="h2">
                                        Loại Menu
                                    </Typography>
                                </Grid>
                                <Grid item xs={8} md={8} sm={8} lg={8} >
                                    <Select
                                        value={menu}
                                        fullWidth
                                        disabled
                                        sx={{ backgroundColor: "#f3f3f3" }}
                                    >
                                        <MenuItem value={"S"}>Small</MenuItem>
                                        <MenuItem value={"M"}>Medium</MenuItem>
                                        <MenuItem value={"L"}>Large</MenuItem>
                                    </Select>
                                </Grid>
                                <Grid item xs={4} md={4} sm={4} lg={4} marginY={"auto"}>
                                    <Typography variant="h6" component="h2">
                                        Đường Kính
                                    </Typography>
                                </Grid>
                                <Grid item xs={8} md={8} sm={8} lg={8}>
                                    <OutlinedInput value={duongKinh} fullWidth disabled sx={{ backgroundColor: "#f3f3f3" }} />
                                </Grid>
                                <Grid item xs={4} md={4} sm={4} lg={4} marginY={"auto"}>
                                    <Typography variant="h6" component="h2">
                                        Sườn
                                    </Typography>
                                </Grid>
                                <Grid item xs={8} md={8} sm={8} lg={8}>
                                    <OutlinedInput value={suonNuong} fullWidth disabled sx={{ backgroundColor: "#f3f3f3" }} />
                                </Grid>
                                <Grid item xs={4} md={4} sm={4} lg={4} marginY={"auto"}>
                                    <Typography variant="h6" component="h2">
                                        Salad
                                    </Typography>
                                </Grid>
                                <Grid item xs={8} md={8} sm={8} lg={8}>
                                    <OutlinedInput value={salad} fullWidth disabled sx={{ backgroundColor: "#f3f3f3" }} />
                                </Grid>
                                <Grid item xs={4} md={4} sm={4} lg={4} marginY={"auto"}>
                                    <Typography variant="h6" component="h2">
                                        Số lượng nước
                                    </Typography>
                                </Grid>
                                <Grid item xs={8} md={8} sm={8} lg={8} >
                                    <OutlinedInput value={soLuongNuoc} fullWidth disabled sx={{ backgroundColor: "#f3f3f3" }} />
                                </Grid>
                                <Grid item xs={4} md={4} sm={4} lg={4} marginY={"auto"}>
                                    <Typography variant="h6" component="h2">
                                        Thành tiền
                                    </Typography>
                                </Grid>
                                <Grid item xs={8} md={8} sm={8} lg={8} >
                                    <OutlinedInput value={thanhTien} fullWidth disabled sx={{ backgroundColor: "#f3f3f3" }} />
                                </Grid>
                                <Grid item xs={4} md={4} sm={4} lg={4} marginY={"auto"}>
                                    <Typography variant="h6" component="h2">
                                        Giảm giá
                                    </Typography>
                                </Grid>
                                <Grid item xs={8} md={8} sm={8} lg={8} >
                                    <OutlinedInput value={giamGia} fullWidth disabled sx={{ backgroundColor: "#f3f3f3" }} />
                                </Grid>
                                <Grid item xs={4} md={4} sm={4} lg={4} marginY={"auto"}>
                                    <Typography variant="h6" component="h2">
                                        Loại pizza
                                    </Typography>
                                </Grid>
                                <Grid item xs={8} md={8} sm={8} lg={8}>
                                    <Select
                                        value={pizzaType}
                                        fullWidth
                                        disabled
                                        sx={{ backgroundColor: "#f3f3f3" }}
                                    >

                                        <MenuItem value={"Seafood"}>seafood</MenuItem>
                                        <MenuItem value={"Hawaii"}>hawaii</MenuItem>
                                        <MenuItem value={"Bacon"}>bacon</MenuItem>
                                    </Select>
                                </Grid>
                            </Grid>
                        </Grid>
                        <Grid item xs={6} md={6} sm={6} lg={6} p={1}>
                            <Grid container spacing={1}>
                                <Grid item xs={4} md={4} sm={4} lg={4} marginY={"auto"}>
                                    <Typography variant="h6" component="h2">
                                        Voucher
                                    </Typography>
                                </Grid>
                                <Grid item xs={8} md={8} sm={8} lg={8}>
                                    <OutlinedInput value={voucher} fullWidth disabled sx={{ backgroundColor: "#f3f3f3" }} />
                                </Grid>
                                <Grid item xs={4} md={4} sm={4} lg={4} marginY={"auto"}>
                                    <Typography variant="h6" component="h2">
                                        Loại Nước Uống
                                    </Typography>
                                </Grid>
                                <Grid item xs={8} md={8} sm={8} lg={8}>
                                    <Select
                                        value={drink}
                                        fullWidth
                                        disabled
                                        sx={{ backgroundColor: "#f3f3f3" }}
                                    >
                                        {
                                            drinkList.map((drink, index) => (
                                                <MenuItem key={index} value={drink.maNuocUong}>{drink.tenNuocUong}</MenuItem>
                                            ))
                                        }
                                    </Select>
                                </Grid>
                                <Grid item xs={4} md={4} sm={4} lg={4} marginY={"auto"}>
                                    <Typography variant="h6" component="h2">
                                        Họ tên
                                    </Typography>
                                </Grid>
                                <Grid item xs={8} md={8} sm={8} lg={8}>
                                    <OutlinedInput value={name} fullWidth disabled sx={{ backgroundColor: "#f3f3f3" }} />
                                </Grid>
                                <Grid item xs={4} md={4} sm={4} lg={4} marginY={"auto"}>
                                    <Typography variant="h6" component="h2">
                                        Email
                                    </Typography>
                                </Grid>
                                <Grid item xs={8} md={8} sm={8} lg={8}>
                                    <OutlinedInput value={email} fullWidth disabled sx={{ backgroundColor: "#f3f3f3" }} />
                                </Grid>
                                <Grid item xs={4} md={4} sm={4} lg={4} marginY={"auto"}>
                                    <Typography variant="h6" component="h2">
                                        Số điện thoại
                                    </Typography>
                                </Grid>
                                <Grid item xs={8} md={8} sm={8} lg={8}>
                                    <OutlinedInput value={phoneNumber} fullWidth disabled sx={{ backgroundColor: "#f3f3f3" }} />
                                </Grid>
                                <Grid item xs={4} md={4} sm={4} lg={4} marginY={"auto"}>
                                    <Typography variant="h6" component="h2">
                                        Địa chỉ
                                    </Typography>
                                </Grid>
                                <Grid item xs={8} md={8} sm={8} lg={8}>
                                    <OutlinedInput value={address} fullWidth disabled sx={{ backgroundColor: "#f3f3f3" }} />
                                </Grid>
                                <Grid item xs={4} md={4} sm={4} lg={4} marginY={"auto"}>
                                    <Typography variant="h6" component="h2">
                                        Lời nhắn
                                    </Typography>
                                </Grid>
                                <Grid item xs={8} md={8} sm={8} lg={8}>
                                    <OutlinedInput value={message} fullWidth disabled sx={{ backgroundColor: "#f3f3f3" }} />
                                </Grid>
                                <Grid item xs={4} md={4} sm={4} lg={4} marginY={"auto"}>
                                    <Typography variant="h6" component="h2">
                                        Trạng thái
                                    </Typography>
                                </Grid>
                                <Grid item xs={8} md={8} sm={8} lg={8} >
                                    <Select
                                        value={orderStatus}
                                        onChange={handleChangeStatus}
                                        fullWidth
                                    >
                                        <MenuItem value={"open"}>Open</MenuItem>
                                        <MenuItem value={"cancel"}>Cancel</MenuItem>
                                        <MenuItem value={"confirmed"}>Confirmed</MenuItem>
                                    </Select>
                                </Grid>
                            </Grid>

                        </Grid>
                    </Grid>

                    <Grid container mt={3}>
                        <Grid item xs={6} md={6} sm={6} lg={6} textAlign="center">
                            <Button color="success" variant="contained" onClick={onBtnUpdateClick}>Update Order</Button>
                        </Grid>
                        <Grid item xs={6} md={6} sm={6} lg={6} textAlign="center">
                            <Button color="success" variant="contained" onClick={handleCloseEdit}>Hủy bỏ</Button>
                        </Grid>
                    </Grid>
                </Box>
            </Modal>
            <Snackbar open={openAlert} autoHideDuration={6000} onClose={handleCloseAlert}>
                <Alert onClose={handleCloseAlert} severity={statusModal} sx={{ width: '100%' }}>
                    {noidungAlert}
                </Alert>
            </Snackbar>
        </Container>
    )
}
export default UpdateModal;