import { Container, Grid,Pagination, Select, MenuItem, InputLabel, Typography, Button, TableCell, TableRow, Paper, TableContainer, Table, TableHead, TableBody } from "@mui/material";
import { useEffect, useState } from "react";
import AddModal from "./AddModal";
import DeleteModal from "./DeleteModal";
import UpdateModal from "./UpdateModal";

function UserList() {
    const style = {
        position: 'absolute',
        top: '50%',
        left: '50%',
        transform: 'translate(-50%, -50%)',
        width: 400,
        bgcolor: 'background.paper',
        border: '2px solid #000',
        boxShadow: 24,
        p: 4,
    };
    const [rows, setRows] = useState([]);
    //Limit: số lượng bản ghi trên 1 trang
    const [limit, setLimit] = useState(10);
    //số trang: tổng số lượng sản phẩm / limit - Số lớn hơn gần nhất
    const [noPage, setNoPage] = useState(0);
    //Trang hiện tại 
    const [page, setPage] = useState(1);

    const [varRefeshPage, setVarRefeshPage] = useState(0);

    const [openModalDelete, setOpenModalDelete]=useState(false);
    const [openModalAdd, setOpenModalAdd] = useState(false);
    const [openModalEdit, setOpenModalEdit] = useState(false);
    const [drinkList, setDrinkList] = useState([]);
    

    const [idDelete, setIdDelete]= useState("");
    const [rowClicked, setRowClicked] = useState([]);

    const handleCloseDelete = () => setOpenModalDelete(false);
    const handleCloseAdd = () => setOpenModalAdd(false);
    const handleCloseEdit = () => setOpenModalEdit(false);

    const getData = async (url, body) => {
        const response = await fetch(url, body);
        const data = await response.json();
        return data;
    }
    useEffect(() => {
        getData("http://42.115.221.44:8080/devcamp-pizza365/orders")
            .then((res) => {

                setNoPage(Math.ceil(res.length / limit));
                setRows(res.slice((page - 1) * limit, page * limit));
            })
            .catch((error) => {
                console.error(error.message)
            })
    }, [page, limit, varRefeshPage])

    useEffect(() => {
        getData("http://42.115.221.44:8080/devcamp-pizza365/drinks")
            .then((res) => {

                setDrinkList(res)
            })
            .catch((error) => {
                console.error(error.message)
            })
    }, [])
    const handleChangeLimit = (event) => {
        setLimit(event.target.value);
    };
    const onChangePagination = (event, value) => {
        setPage(value);
    }
    const onBtnUpdateHandler = (row) => {

        setOpenModalEdit(true)
        setRowClicked(row)
    }


    const onBtnDeleteClick = (row) => {

        setOpenModalDelete(true)
        setIdDelete(row.id)
    }
    const onBtnAddUserClick = () =>{

        setOpenModalAdd(true)
    }

    return (
        <Container >
            <Grid container spacing={2}>
                <Grid mt={5} item xs={12} sm={12} md={12} lg={12} sx={{ textAlign: "center" }}>
                    <Typography variant="h3" component="div">
                        Danh sách đơn hàng
                    </Typography>
                </Grid>
                <Grid item xs={12} sm={12} md={12} lg={12}>
                    <Button variant="contained" color="success" onClick={onBtnAddUserClick} >Thêm đơn hàng</Button>
                </Grid>
                <Grid container mt={3} mb={2} justifyContent="flex-end">
                    <Grid item marginY={"auto"} mr={1}>
                        <InputLabel>Show</InputLabel>
                    </Grid>
                    <Grid item>
                        <Select
                            labelId="demo-simple-select-label"
                            id="demo-simple-select"
                            value={limit}
                            onChange={handleChangeLimit}
                        >
                            <MenuItem value={5}>5</MenuItem>
                            <MenuItem value={10}>10</MenuItem>
                            <MenuItem value={25}>25</MenuItem>
                            <MenuItem value={50}>50</MenuItem>
                        </Select>
                    </Grid>
                    <Grid item marginY={"auto"} ml={1}>
                        <InputLabel>entries</InputLabel>
                    </Grid>
                </Grid>
                <TableContainer component={Paper}>
                    <Table sx={{ minWidth: 650 }} aria-label="simple table">
                        <TableHead sx={{ backgroundColor: "#d5d9dc" }}>
                            <TableRow>
                                <TableCell align="center">Order id</TableCell>
                                <TableCell align="center">Họ Tên</TableCell>
                                <TableCell align="center">Số điện thoại</TableCell>
                                <TableCell align="center">Địa chỉ</TableCell>
                                <TableCell align="center">Loại menu</TableCell>
                                <TableCell align="center">Thành tiền</TableCell>
                                <TableCell align="center">Loại pizza</TableCell>
                                <TableCell align="center">Loại nước uống</TableCell>
                                <TableCell align="center">Voucher</TableCell>
                                <TableCell align="center">Giảm giá</TableCell>
                                <TableCell align="center">Trạng thái</TableCell>
                                <TableCell align="center">Action</TableCell>
                            </TableRow>
                        </TableHead>
                        <TableBody>
                            {rows.map((row, index) => (
                                <TableRow
                                    key={index}
                                    sx={{ '&:last-child td, &:last-child th': { border: 0 } }}
                                >
                                    <TableCell align="center">{row.orderId}</TableCell>
                                    <TableCell align="center">{row.hoTen}</TableCell>
                                    <TableCell align="center">{row.soDienThoai}</TableCell>
                                    <TableCell align="center">{row.diaChi}</TableCell>
                                    <TableCell align="center">{row.kichCo}</TableCell>
                                    <TableCell align="center">{row.thanhTien}</TableCell>
                                    <TableCell align="center">{row.loaiPizza}</TableCell>
                                    <TableCell align="center">{row.idLoaiNuocUong}</TableCell>
                                    <TableCell align="center">{row.idVourcher}</TableCell>
                                    <TableCell align="center">{row.giamGia}</TableCell>
                                    <TableCell align="center">{row.trangThai}</TableCell>
                                    <TableCell align="center">
                                        <Button variant="contained" color="primary" onClick={() => onBtnUpdateHandler(row)} >Update</Button>
                                        <Button variant="contained" color="error" onClick={() => onBtnDeleteClick(row)} >Delete</Button>
                                    </TableCell>
                                </TableRow>
                            ))}
                        </TableBody>
                    </Table>
                </TableContainer>
                <Grid container mt={3} mb={2}  justifyContent="flex-end">
                    <Grid item >
                        <Pagination count={noPage} defaultPage={1} onChange={onChangePagination} />
                    </Grid>
                </Grid>
            </Grid>
            <DeleteModal varRefeshPage={varRefeshPage} setVarRefeshPage={setVarRefeshPage} style={style} openModalDelete={openModalDelete} idDelete={idDelete} handleCloseDelete={handleCloseDelete}/>
            <AddModal drinkList={drinkList} varRefeshPage={varRefeshPage} setOpenModalAdd={setOpenModalAdd} openModalAdd={openModalAdd} handleCloseAdd={handleCloseAdd} style={style} getData={getData} setVarRefeshPage={setVarRefeshPage}/>
            <UpdateModal drinkList={drinkList} varRefeshPage={varRefeshPage} openModalEdit={openModalEdit} handleCloseEdit={handleCloseEdit}
             style={style} getData={getData} setVarRefeshPage={setVarRefeshPage} rowClicked={rowClicked}
             />
        </Container>
    )
}
export default UserList;