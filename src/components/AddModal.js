import { Button, Container, Grid, Typography, Modal, Box, OutlinedInput, MenuItem, Select, Snackbar, Alert } from "@mui/material";
import { useState } from "react";


function AddModal({drinkList, openModalAdd, setOpenModalAdd, handleCloseAdd, style, getData, setVarRefeshPage, varRefeshPage }) {
    const menuDetail = [
        {
            menuName: "S",    // S, M, L
            duongKinh: "20",
            suonNuong: 2,
            salad: 200,
            drink: 2,
            price: 150000

        },
        {
            menuName: "M",    // S, M, L
            duongKinh: "25",
            suonNuong: 4,
            salad: 300,
            drink: 3,
            price: 200000

        },
        {
            menuName: "L",    // S, M, L
            duongKinh: "30",
            suonNuong: 8,
            salad: 500,
            drink: 4,
            price: 250000

        }
    ]
    const [discount, setDiscount] = useState([])
    const [menu, setMenu] = useState('');
    const [pizzaType, setPizzaType] = useState("Seafood");
    const [duongKinh, setDuongKinh] = useState("");
    const [suonNuong, setSuonNuong] = useState("");
    const [salad, setSalad] = useState("");
    const [soLuongNuoc, setSoLuongNuoc] = useState("");
    const [thanhTien, setThanhTien] = useState("");
    const [voucher, setVoucher] = useState("");
    const [drink, setDrink] = useState("");
    const [name, setName] = useState("");
    const [email, setEmail] = useState("");
    const [phoneNumber, setPhoneNumber] = useState("");
    const [address, setAddress] = useState("");
    const [message, setMessage] = useState("");
    const [giamGia, setGiamGia]=useState(0);


    const [openAlert, setOpenAlert] = useState(false)
    const [statusModal, setStatusModal] = useState("error")
    const [noidungAlert, setNoidungAlert] = useState("")

    const handleCloseAlert = () => {
        setOpenAlert(false)
    }

    const validateDataForm = (paramDataForm) => {
        if(paramDataForm.kichCo === "") {
            setOpenAlert(true);
            setStatusModal("error")
            setNoidungAlert("Chưa chọn kiểu menu!");
            return false;
        }
        if(paramDataForm.loaiPizza === "") {
            setOpenAlert(true);
            setStatusModal("error")
            setNoidungAlert("Chưa chọn loại Pizza!");
            return false;
        }
        if(paramDataForm.idVourcher!==""&& isNaN(paramDataForm.idVourcher)) {
            setOpenAlert(true);
            setStatusModal("error")
            setNoidungAlert("Voucher phải là số!");
            return false;
        }
        else if(paramDataForm.idVourcher!==""){
            getData("http://42.115.221.44:8080/devcamp-voucher-api/voucher_detail/"+paramDataForm.idVourcher)
            .then((res) => {
                setDiscount(res.phanTramGiamGia);
                setOpenAlert(true);
                setStatusModal("success");
                setNoidungAlert(`Bạn được giảm giá ${discount} %`);
                setGiamGia(thanhTien*(discount/100));
            })
            .catch((error) => {
            setOpenAlert(true);
            setStatusModal("warning");
            setNoidungAlert("Mã voucher không tồn tại!");
            setVoucher("");
            })
        }
        if(paramDataForm.idLoaiNuocUong === "") {
            setOpenAlert(true);
            setStatusModal("error")
            setNoidungAlert("Chưa chọn loại đồ uống!");
            return false;
        }
        if(paramDataForm.hoTen === "") {
            setOpenAlert(true);
            setStatusModal("error")
            setNoidungAlert("Chưa nhập tên!");
            return false;
        }
        let regex=/^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/;
        if(paramDataForm.email!=="" && !(regex.test(paramDataForm.email))) {
            setOpenAlert(true);
            setStatusModal("error")
            setNoidungAlert("Email không đúng định dạng!");
            return false;
        }
        if(isNaN(paramDataForm.soDienThoai) ||paramDataForm.soDienThoai==="") {
            setOpenAlert(true);
            setStatusModal("error")
            setNoidungAlert("Số điện thoại không đúng định dạng!");
            return false;
        }
    
        if(paramDataForm.diaChi === "") {
            setOpenAlert(true);
            setStatusModal("error")
            setNoidungAlert("Chưa nhập địa chỉ!");
            return false;
        }
        else{
            return true;
        }
    }

    const onBtnInsertClick=()=>{
        var vDataForm = {
            kichCo: menu,
            duongKinh: duongKinh,
            suon: suonNuong,
            salad: salad,
            soLuongNuoc: soLuongNuoc,
            thanhTien: thanhTien,
            loaiPizza: pizzaType,
            idVourcher: voucher,
            idLoaiNuocUong: drink,
            hoTen: name,
            email: email,
            soDienThoai: phoneNumber,
            diaChi: address,
            loiNhan: message,
            giamGia:giamGia
        }
        var vCheckData = validateDataForm(vDataForm);
        if(vCheckData){

            const body = {
                method: 'POST',
                body: JSON.stringify(vDataForm),
                headers: {
                  'Content-type': 'application/json; charset=UTF-8',
                }
            }
            getData('http://42.115.221.44:8080/devcamp-pizza365/orders', body)
            .then((data) =>{
                setOpenAlert(true);
                setStatusModal("success");
                setNoidungAlert("Dữ liệu thêm thành công!");
                closeModal();
                setVarRefeshPage(varRefeshPage + 1)

            })
            .catch((error) =>{
                setOpenAlert(true);
                setStatusModal("error")
                setNoidungAlert("Dữ liệu thêm thất bại!")

            })
        }
    }


    const handleChangeMenu = (event) => {
        setMenu(event.target.value);
        switch (event.target.value) {
            case "S":
                setDuongKinh(menuDetail[0].duongKinh);
                setSuonNuong(menuDetail[0].suonNuong);
                setSalad(menuDetail[0].salad);
                setSoLuongNuoc(menuDetail[0].drink);
                setThanhTien(menuDetail[0].price);
                break;
            case "M":
                setDuongKinh(menuDetail[1].duongKinh);
                setSuonNuong(menuDetail[1].suonNuong);
                setSalad(menuDetail[1].salad);
                setSoLuongNuoc(menuDetail[1].drink);
                setThanhTien(menuDetail[1].price);
                break;
            default:
                setDuongKinh(menuDetail[2].duongKinh);
                setSuonNuong(menuDetail[2].suonNuong);
                setSalad(menuDetail[2].salad);
                setSoLuongNuoc(menuDetail[2].drink);
                setThanhTien(menuDetail[2].price);
                break;
        }
    };
    const handleChangePizzaType = (event) => {
        setPizzaType(event.target.value);
    };
    const handleChangeDrink = (event) => {
        setDrink(event.target.value);
    };
    const onVoucherChange = (event) => {
        setVoucher(event.target.value);
    }
    const onNameChange = (event) => {
        setName(event.target.value);
    }
    const onEmailChange = (event) => {
        setEmail(event.target.value);
    }
    const onPhoneNumberChange = (event) => {
        setPhoneNumber(event.target.value);
    }
    const onAddressChange = (event) => {
        setAddress(event.target.value);
    }
    const onMessageChange = (event) => {
        setMessage(event.target.value);
    }
    const clearModal = () => {
        setMenu("");
        setDuongKinh("");
        setSuonNuong("");
        setSalad("");
        setSoLuongNuoc("");
        setThanhTien("");
        setPizzaType("");
        setVoucher("");
        setDrink("");
        setName("");
        setEmail("");
        setPhoneNumber("");
        setAddress("");
        setMessage("");
    }
    const closeModal = () => {
        clearModal();
        handleCloseAdd();
    }
    return (
        <Container>
            <Modal
                open={openModalAdd}
                onClose={handleCloseAdd}
                aria-labelledby="modal-modal-title"
                aria-describedby="modal-modal-description"
            >
                <Box sx={{ ...style, width: 800 }}>
                    <Typography variant="h5" component="h2" mb={2} textAlign="center">
                        Tạo Đơn Hàng
                    </Typography>
                    <Grid container >
                        <Grid item xs={6} md={6} sm={6} lg={6} p={1}>
                            <Grid container spacing={1}>
                                <Grid item xs={4} md={4} sm={4} lg={4} marginY={"auto"}>
                                    <Typography variant="h6" component="h2">
                                        Loại Menu
                                    </Typography>
                                </Grid>
                                <Grid item xs={8} md={8} sm={8} lg={8}>
                                    <Select
                                        value={menu}
                                        onChange={handleChangeMenu}
                                        fullWidth
                                    >
                                        <MenuItem value={"S"}>Small</MenuItem>
                                        <MenuItem value={"M"}>Medium</MenuItem>
                                        <MenuItem value={"L"}>Large</MenuItem>
                                    </Select>
                                </Grid>
                                <Grid item xs={4} md={4} sm={4} lg={4} marginY={"auto"}>
                                    <Typography variant="h6" component="h2">
                                        Đường Kính
                                    </Typography>
                                </Grid>
                                <Grid item xs={8} md={8} sm={8} lg={8}>
                                    <OutlinedInput value={duongKinh} fullWidth disabled sx={{ backgroundColor: "#f3f3f3" }} />
                                </Grid>
                                <Grid item xs={4} md={4} sm={4} lg={4} marginY={"auto"}>
                                    <Typography variant="h6" component="h2">
                                        Sườn
                                    </Typography>
                                </Grid>
                                <Grid item xs={8} md={8} sm={8} lg={8}>
                                    <OutlinedInput value={suonNuong} fullWidth disabled sx={{ backgroundColor: "#f3f3f3" }} />
                                </Grid>
                                <Grid item xs={4} md={4} sm={4} lg={4} marginY={"auto"}>
                                    <Typography variant="h6" component="h2">
                                        Salad
                                    </Typography>
                                </Grid>
                                <Grid item xs={8} md={8} sm={8} lg={8}>
                                    <OutlinedInput value={salad} fullWidth disabled sx={{ backgroundColor: "#f3f3f3" }} />
                                </Grid>
                                <Grid item xs={4} md={4} sm={4} lg={4} marginY={"auto"}>
                                    <Typography variant="h6" component="h2">
                                        Số lượng nước
                                    </Typography>
                                </Grid>
                                <Grid item xs={8} md={8} sm={8} lg={8} >
                                    <OutlinedInput value={soLuongNuoc} fullWidth disabled sx={{ backgroundColor: "#f3f3f3" }} />
                                </Grid>
                                <Grid item xs={4} md={4} sm={4} lg={4} marginY={"auto"}>
                                    <Typography variant="h6" component="h2">
                                        Thành tiền
                                    </Typography>
                                </Grid>
                                <Grid item xs={8} md={8} sm={8} lg={8} >
                                    <OutlinedInput value={thanhTien} fullWidth disabled sx={{ backgroundColor: "#f3f3f3" }} />
                                </Grid>
                                <Grid item xs={4} md={4} sm={4} lg={4} marginY={"auto"}>
                                    <Typography variant="h6" component="h2">
                                        Loại pizza
                                    </Typography>
                                </Grid>
                                <Grid item xs={8} md={8} sm={8} lg={8}>
                                    <Select
                                        value={pizzaType}
                                        onChange={handleChangePizzaType}
                                        fullWidth
                                    >

                                        <MenuItem value={"Seafood"}>seafood</MenuItem>
                                        <MenuItem value={"Hawaii"}>hawaii</MenuItem>
                                        <MenuItem value={"Bacon"}>bacon</MenuItem>
                                    </Select>
                                </Grid>
                            </Grid>
                        </Grid>
                        <Grid item xs={6} md={6} sm={6} lg={6} p={1}>
                            <Grid container spacing={1}>
                                <Grid item xs={4} md={4} sm={4} lg={4} marginY={"auto"}>
                                    <Typography variant="h6" component="h2">
                                        Voucher
                                    </Typography>
                                </Grid>
                                <Grid item xs={8} md={8} sm={8} lg={8}>
                                    <OutlinedInput value={voucher} onChange={onVoucherChange} fullWidth />
                                </Grid>
                                <Grid item xs={4} md={4} sm={4} lg={4} marginY={"auto"}>
                                    <Typography variant="h6" component="h2">
                                        Loại Nước Uống
                                    </Typography>
                                </Grid>
                                <Grid item xs={8} md={8} sm={8} lg={8}>
                                    <Select
                                        value={drink}
                                        onChange={handleChangeDrink}
                                        fullWidth
                                    >
                                        {
                                            drinkList.map((drink, index) => (
                                                <MenuItem key={index} value={drink.maNuocUong}>{drink.tenNuocUong}</MenuItem>
                                            ))
                                        }
                                    </Select>
                                </Grid>
                                <Grid item xs={4} md={4} sm={4} lg={4} marginY={"auto"}>
                                    <Typography variant="h6" component="h2">
                                        Họ tên
                                    </Typography>
                                </Grid>
                                <Grid item xs={8} md={8} sm={8} lg={8}>
                                    <OutlinedInput value={name} onChange={onNameChange} fullWidth />
                                </Grid>
                                <Grid item xs={4} md={4} sm={4} lg={4} marginY={"auto"}>
                                    <Typography variant="h6" component="h2">
                                        Email
                                    </Typography>
                                </Grid>
                                <Grid item xs={8} md={8} sm={8} lg={8}>
                                    <OutlinedInput value={email} onChange={onEmailChange} fullWidth />
                                </Grid>
                                <Grid item xs={4} md={4} sm={4} lg={4} marginY={"auto"}>
                                    <Typography variant="h6" component="h2">
                                        Số điện thoại
                                    </Typography>
                                </Grid>
                                <Grid item xs={8} md={8} sm={8} lg={8}>
                                    <OutlinedInput value={phoneNumber} onChange={onPhoneNumberChange} fullWidth />
                                </Grid>
                                <Grid item xs={4} md={4} sm={4} lg={4} marginY={"auto"}>
                                    <Typography variant="h6" component="h2">
                                        Địa chỉ
                                    </Typography>
                                </Grid>
                                <Grid item xs={8} md={8} sm={8} lg={8}>
                                    <OutlinedInput value={address} onChange={onAddressChange} fullWidth />
                                </Grid>
                                <Grid item xs={4} md={4} sm={4} lg={4} marginY={"auto"}>
                                    <Typography variant="h6" component="h2">
                                        Lời nhắn
                                    </Typography>
                                </Grid>
                                <Grid item xs={8} md={8} sm={8} lg={8}>
                                    <OutlinedInput value={message} onChange={onMessageChange} fullWidth />
                                </Grid>
                            </Grid>

                        </Grid>
                    </Grid>

                    <Grid container mt={3}>
                        <Grid item xs={6} md={6} sm={6} lg={6} textAlign="center">
                            <Button color="success" variant="contained" onClick={onBtnInsertClick}>Insert Order</Button>
                        </Grid>
                        <Grid item xs={6} md={6} sm={6} lg={6} textAlign="center">
                            <Button color="success" variant="contained" onClick={closeModal}>Hủy bỏ</Button>
                        </Grid>
                    </Grid>
                </Box>
            </Modal>
            <Snackbar open={openAlert} autoHideDuration={6000} onClose={handleCloseAlert}>
                <Alert onClose={handleCloseAlert} severity={statusModal} sx={{ width: '100%' }}>
                    {noidungAlert}
                </Alert>
            </Snackbar>
        </Container>
    )
}
export default AddModal;