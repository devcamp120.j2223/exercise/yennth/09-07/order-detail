import { Button, Container, Grid, Typography, Modal, Box, Snackbar, Alert } from "@mui/material";
import { useState } from "react";


function DeleteModal({ openModalDelete, idDelete, handleCloseDelete, style, setVarRefeshPage, varRefeshPage }) {
    const [openAlert, setOpenAlert] = useState(false)
    const [statusModalDelete, setStatusModalDelete] = useState("error")
    const [noidungAlert, setNoidungAlert] = useState("")

    const handleCloseAlert = () => {
        setOpenAlert(false)
    }

    const onBtnConfirmDeleteClick = () => {
        const vURL_DELETE = 'http://42.115.221.44:8080/devcamp-pizza365/orders/' + idDelete;
        fetch(vURL_DELETE, { method: 'DELETE' })
            .then(async response => {
                const isJson = response.headers.get('content-type')?.includes('application/json');
                const data = isJson && await response.json();
                // check for error response
                if (!response.ok) {
                    // get error message from body or default to response status
                    const error = (data && data.message) || response.status;
                    return Promise.reject(error);
                }
         
                setOpenAlert(true)
                setStatusModalDelete("success")
                setNoidungAlert("Xóa Đơn hàng " + idDelete + " thành công!")
                setVarRefeshPage(varRefeshPage + 1)
                handleCloseDelete()
            })
            .catch(error => {
                console.error('There was an error!', error);
                setOpenAlert(true)
                setStatusModalDelete("error")
                setNoidungAlert("Xóa Đơn hàng " + { idDelete } + " thất bại!")
                handleCloseDelete()
            });
    }

    return (
        <Container>
            <Modal
                open={openModalDelete}
                onClose={handleCloseDelete}
                aria-labelledby="modal-modal-title"
                aria-describedby="modal-modal-description"
            >
                <Box sx={style}>
                    <Typography variant="h5" component="h2">
                        Thêm User
                    </Typography>
                    <Typography mt={2}>
                        Bạn có chắc chắn muốn xóa đơn hàng này này không?
                    </Typography>
                    <Grid container mt={3}>
                        <Grid item xs={6} md={6} sm={6} lg={6} textAlign="center">
                            <Button color="error" variant="contained" onClick={onBtnConfirmDeleteClick}>Xác nhận</Button>
                        </Grid>
                        <Grid item xs={6} md={6} sm={6} lg={6} textAlign="center">
                            <Button color="inherit" variant="contained" onClick={handleCloseDelete}>Hủy bỏ</Button>
                        </Grid>
                    </Grid>
                </Box>
            </Modal>
            <Snackbar open={openAlert} autoHideDuration={6000} onClose={handleCloseAlert}>
                <Alert onClose={handleCloseAlert} severity={statusModalDelete} sx={{ width: '100%' }}>
                    {noidungAlert}
                </Alert>
            </Snackbar>
        </Container>
    )
}
export default DeleteModal;